from django.contrib import admin
from .models import *


class CategoryAdmin(admin.ModelAdmin):
    search_fields = ['title','body']
    
admin.site.register(Category, CategoryAdmin)
admin.site.register(Ingredient)

admin.site.register(Recipe)

class UserAdmin(admin.ModelAdmin):
    def has_add_permission(self, request, obj = None):
        return False
    def has_change_permission(self, request, obj = None):
        return False

admin.site.register(User, UserAdmin)