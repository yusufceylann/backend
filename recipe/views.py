from django.shortcuts import render
from recipe.models import User, Recipe, Ingredient, Category
from rest_framework import viewsets
from rest_framework import permissions
from recipe.serializers import UserSerializer, RecipeSerializer,CategorySerializer, IngredientSerializer
from rest_framework.response import Response

class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class RecipeViewSet(viewsets.ModelViewSet):
    # print("*************DEBUG 1****************")
    queryset = Recipe.objects.all()
    serializer_class = RecipeSerializer
  
    def list(self,request,id=None):
        return Response(RecipeSerializer(Recipe.objects.filter(category = id), many = True,).data)


class IngredientViewSet(viewsets.ModelViewSet):
    queryset = Ingredient.objects.all()
    serializer_class = IngredientSerializer

class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.filter(parent=None).prefetch_related("children")
    serializer_class = CategorySerializer



    #permission_classes = [permissions.IsAuthenticated]
